%dw 2.0
import * from dw::test::Asserts
---
payload must [
  haveKey("appName"),
  haveKey ("appVersion"),
  $.appStatus must equalTo("RUNNING"),
  haveKey("timestamp")
]